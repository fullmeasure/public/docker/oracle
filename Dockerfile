FROM registry.gitlab.com/fullmeasure/public/docker/sidekiq:latest

MAINTAINER FME <dev@fullmeasureed.com>

# Install packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y unzip && \
    rm -rf /var/lib/apt/lists/*

# SETUP CHECKOUT
COPY *.zip /tmp/

# SETUP ORACLE
RUN unzip -qq /tmp/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /tmp/ && \
    unzip -qq /tmp/instantclient-sqlplus-linux.x64-12.2.0.1.0.zip -d /tmp/ && \
    unzip -qq /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /tmp/ && \
    mkdir -p /usr/local/oracle/admin/network && \
    mkdir -p /usr/local/oracle/product/instantclient_64/12.2.0.1.0/bin && \
    mkdir -p /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib && \
    mkdir -p /usr/local/oracle/product/instantclient_64/12.2.0.1.0/jdbc/lib && \
    mkdir -p /usr/local/oracle/product/instantclient_64/12.2.0.1.0/rdbms/jlib && \
    mkdir -p /usr/local/oracle/product/instantclient_64/12.2.0.1.0/sqlplus/admin && \
    mv /tmp/instantclient_12_2/ojdbc* /usr/local/oracle/product/instantclient_64/12.2.0.1.0/jdbc/lib/ && \
    mv /tmp/instantclient_12_2/x*.jar /usr/local/oracle/product/instantclient_64/12.2.0.1.0/rdbms/jlib/ && \
    mv /tmp/instantclient_12_2/glogin.sql /usr/local/oracle/product/instantclient_64/12.2.0.1.0/sqlplus/admin/login.sql && \
    mv /tmp/instantclient_12_2/*so* /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib/ && \
    mv /tmp/instantclient_12_2/sdk /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib/sdk && \
    mv /tmp/instantclient_12_2/*README /usr/local/oracle/product/instantclient_64/12.2.0.1.0/ && \
    mv /tmp/instantclient_12_2/* /usr/local/oracle/product/instantclient_64/12.2.0.1.0/bin/ && \
    ln -s /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib/libclntsh.so.12.1 /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib/libclntsh.so && \
    ln -s /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib/libocci.so.12.1 /usr/local/oracle/product/instantclient_64/12.2.0.1.0/lib/libocci.so && \
    rm -rf /tmp/*

ENV ORACLE_BASE=/usr/local/oracle
ENV ORACLE_HOME=$ORACLE_BASE/product/instantclient_64/12.2.0.1.0
ENV PATH=$ORACLE_HOME/bin:$PATH
ENV LD_LIBRARY_PATH=$ORACLE_HOME/lib:$LD_LIBRARY_PATH
ENV TNS_ADMIN=$ORACLE_BASE/admin/network
ENV SQLPATH=$ORACLE_HOME/sqlplus/admin
ENV NLS_LANG=AMERICAN_AMERICA.UTF8
